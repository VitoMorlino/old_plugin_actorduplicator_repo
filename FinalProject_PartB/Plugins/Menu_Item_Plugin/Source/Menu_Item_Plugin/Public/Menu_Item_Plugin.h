// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "ModuleManager.h"

#include "MyWindow.h"
#include <memory>

class FMenuBuilder; //Declare class that provides members for building menu items

TUniquePtr<MyWindow> WindowPtr(new MyWindow);

class FMenu_Item_PluginModule : public IModuleInterface
{
public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

private:

	TSharedPtr<class FUICommandList> PluginCommands; // Used to map items to commands/actions

	void AddMenuExtension(FMenuBuilder& Builder); //Used to call an FMenuBuilder function to decorate a menu item with a name, label, and behavior
	void DisplayWindow(); //Used to display a message when called from the menu item
};