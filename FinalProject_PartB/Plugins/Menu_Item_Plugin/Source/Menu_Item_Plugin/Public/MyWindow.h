#pragma once

#include "CoreMinimal.h"

class MyWindow
{
public:
	MyWindow(); //Constructor
	~MyWindow(); //Deconstructor

	void RegisterTab();
	void SpawnTab();
	void UnregisterTab();

	FReply MyWindow::DuplicateButtonPressed();

	/* Checkboxes */

	TSharedPtr<SCheckBox> CheckBoxXPos;
	TSharedPtr<SCheckBox> CheckBoxXNeg;

	TSharedPtr<SCheckBox> CheckBoxYPos;
	TSharedPtr<SCheckBox> CheckBoxYNeg;

	TSharedPtr<SCheckBox> CheckBoxZPos;
	TSharedPtr<SCheckBox> CheckBoxZNeg;

	/* Checkbox State Toggles*/

	void ToggleCheckBoxXPos(ECheckBoxState NewState);
	void ToggleCheckBoxXNeg(ECheckBoxState NewState);

	void ToggleCheckBoxYPos(ECheckBoxState NewState);
	void ToggleCheckBoxYNeg(ECheckBoxState NewState);

	void ToggleCheckBoxZPos(ECheckBoxState NewState);
	void ToggleCheckBoxZNeg(ECheckBoxState NewState);

	/* Num Copies Get/Set */

	TOptional<int32> GetNumCopies() const;
	
	void SetNumCopies(int32 NewValue);

	/* Spacing Amount Get/Set */

	TOptional<float> GetSpacingAmountX() const;
	TOptional<float> GetSpacingAmountY() const;
	TOptional<float> GetSpacingAmountZ() const;

	void SetSpacingAmountX(float NewValue);
	void SetSpacingAmountY(float NewValue);
	void SetSpacingAmountZ(float NewValue);

	/* Actor Selection */

	UWorld* MyWorld();

	FText GetActorName() const;

	bool IsActorSelected(AStaticMeshActor* SelectedActor);
	void ActorSelected(AStaticMeshActor* SelectedActor);

	void DeselectActors();

	TSharedRef<SWidget> DisplayActorList();

private:

	TSharedRef<class SDockTab> OnSpawnPluginTab(const class FSpawnTabArgs& SpawnTabArgs);

	/* Num Copies */

	int32 NumCopies;

	/* Axis Checkboxes */

	bool IsCheckedXPos;
	bool IsCheckedXNeg;

	bool IsCheckedYPos;
	bool IsCheckedYNeg;

	bool IsCheckedZPos;
	bool IsCheckedZNeg;

	/* Spacing Amount */

	float SpacingAmountX;
	float SpacingAmountY;
	float SpacingAmountZ;

	/* Actor Selection */

	TSharedPtr<SComboButton> ActorDropdown;
	AStaticMeshActor* MyActor;

	/* Actor Spawning */

	FActorSpawnParameters SpawnParams;
	AStaticMeshActor* MyActorCopy;
	float MyActorCopyX;
	float MyActorCopyY;
	float MyActorCopyZ;
	FVector GroundOrigin;
	FVector GroundExtends;
	int CopyCount;
	FString StringCopyCount;
	FString MyActorName;
	FString NewName;
};