
#include "MenuStyle.h"
#include "SlateGameResources.h"
#include "IPluginManager.h"
#include "VitosLogMacros.h"

TSharedPtr< FSlateStyleSet > FMenuStyle::StyleInstance = NULL;

void FMenuStyle::Initialize()
{
	if (!StyleInstance.IsValid())
	{
		StyleInstance = Create();
		FSlateStyleRegistry::RegisterSlateStyle(*StyleInstance);
	}
}

void FMenuStyle::Shutdown()
{
	FSlateStyleRegistry::UnRegisterSlateStyle(*StyleInstance);
	ensure(StyleInstance.IsUnique());
	StyleInstance.Reset();
}

FName FMenuStyle::GetStyleSetName()
{
	static FName StyleSetName(TEXT("MenuStyle"));
	return StyleSetName;
}

#define IMAGE_BRUSH( RelativePath, ... ) FSlateImageBrush( Style->RootToContentDir( RelativePath, TEXT(".png") ), __VA_ARGS__ )
#define BOX_BRUSH( RelativePath, ... ) FSlateBoxBrush( Style->RootToContentDir( RelativePath, TEXT(".png") ), __VA_ARGS__ )
#define BORDER_BRUSH( RelativePath, ... ) FSlateBorderBrush( Style->RootToContentDir( RelativePath, TEXT(".png") ), __VA_ARGS__ )
#define TTF_FONT( RelativePath, ... ) FSlateFontInfo( Style->RootToContentDir( RelativePath, TEXT(".ttf") ), __VA_ARGS__ )
#define OTF_FONT( RelativePath, ... ) FSlateFontInfo( Style->RootToContentDir( RelativePath, TEXT(".otf") ), __VA_ARGS__ )

const FVector2D Icon16x16(16.0f, 16.0f);
const FVector2D Icon20x20(20.0f, 20.0f);
const FVector2D Icon40x40(40.0f, 40.0f);

TSharedRef< FSlateStyleSet > FMenuStyle::Create()
{
	//Create HelpMenu Slate Style
	TSharedRef< FSlateStyleSet > Style = MakeShareable(new FSlateStyleSet("MenuStyle"));

	//Find HelpMenu plugin location and set directory to its Resources folder
	Style->SetContentRoot(IPluginManager::Get().FindPlugin("Menu_Item_Plugin")->GetBaseDir() / TEXT("Resources"));

	//Set icon of each MenuItem to their respective icons
	Style->Set("Menu_Item_Plugin.MenuItem1", new IMAGE_BRUSH(TEXT("icon40"), Icon40x40));

	//Return the style we just built
	return Style;
}

#undef IMAGE_BRUSH
#undef BOX_BRUSH
#undef BORDER_BRUSH
#undef TTF_FONT
#undef OTF_FONT

void FMenuStyle::ReloadTextures()
{
	FSlateApplication::Get().GetRenderer()->ReloadTextureResources();
}

const ISlateStyle& FMenuStyle::Get()
{
	return *StyleInstance;
}
