// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Menu_Item_Plugin.h"
#include "VitosLogMacros.h"
#include "LevelEditor.h" // Provides members for accessing unreal editor to add custom interface elements
#include "Framework/MultiBox/MultiBoxBuilder.h" // Identifies the Builder parameter reference

#include "MenuStyle.h"
#include "MenuCommands.h"

#define LOCTEXT_NAMESPACE "FMenu_Item_PluginModule"

void FMenu_Item_PluginModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module

	//Init & Load textures from style
	FMenuStyle::Initialize();
	FMenuStyle::ReloadTextures();

	//Registers commands with a name, tooltip, action type, and hotkey gesture
	FMenuCommands::Register();

	PluginCommands = MakeShareable(new FUICommandList);

	PluginCommands->MapAction(
		FMenuCommands::Get().MenuItem1,
		FExecuteAction::CreateRaw(this, &FMenu_Item_PluginModule::DisplayWindow), FCanExecuteAction());

	//Reference used to call a LevelEditor module member function
	FLevelEditorModule& LevelEditorModule = FModuleManager::LoadModuleChecked<FLevelEditorModule>("LevelEditor");

	//Pointer to an FExtender object that provides access to pull-down menus
	TSharedPtr<FExtender> MenuExtender = MakeShareable(new FExtender());

	//Used to access a specific pull-down menu
	MenuExtender->AddMenuExtension(
		"WindowLayout", // Section to put menu items
		EExtensionHook::After, // Where in section to put menu items
		PluginCommands.ToSharedRef(), // Action mappings for commands
		FMenuExtensionDelegate::CreateRaw(this, &FMenu_Item_PluginModule::AddMenuExtension));

	LevelEditorModule.GetMenuExtensibilityManager()->AddExtender(MenuExtender);

	WindowPtr->RegisterTab();
}

void FMenu_Item_PluginModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.

	WindowPtr->UnregisterTab();
}

void FMenu_Item_PluginModule::AddMenuExtension(FMenuBuilder& Builder)
{
	//Adds a basic horiz line separator
	Builder.AddMenuSeparator();

	//Adds the menu item to the pull-down including the name, tooltip, icon, and execute action
	Builder.AddMenuEntry(FMenuCommands::Get().MenuItem1);
}

void FMenu_Item_PluginModule::DisplayWindow()
{
	LOGMSG(LogTemp, Warning, "Displaying Window");

	WindowPtr->SpawnTab();
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FMenu_Item_PluginModule, Menu_Item_Plugin)